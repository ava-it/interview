# Мануал на Русском
https://habr.com/ru/companies/flant/articles/496936/

# Офф дока. Полезно!
https://cert-manager.io/docs/configuration/acme/

# Установка cert-manager
```sh
wget https://github.com/jetstack/cert-manager/releases/download/v1.13.1/cert-manager.yaml
```
### После скачаивания установить 
- nodeSelector:
- tolerations:

### Дальше применяем настройки создания сертов в clusterIssuer
На выбор. Или совместно.
- acme
- dns здесь нужно создать токен на cloudflare

### После применения конфигов нужно проверить что все отработало
```sh
k -n cert-manager-stage get certificate
k -n cert-manager-stage describe certificate acme-letsencrypt-dns
k -n cert-manager-stage describe certificaterequest acme-letsencrypt-dns-1
```
Как статус будет сообщать что запрос прошел успешно. Проверяем валидность для любого поддомена
```sh
kubectl -n cert-manager-stage get secret acme-letsencrypt-tls-dns -ojson | jq -r '.data."tls.crt"' | base64 -d | openssl x509 -dates -noout -text |grep DNS:
          DNS:*.devopsit.ru, DNS:devopsit.ru
```

### Проверка сертика
```sh
k -n cert-manager get secret acme-letsencrypt-tls -ojson | jq -r '.data."tls.crt"' | base64 -d | openssl x509 -dates -noout -issuer
```
### Проверка что создание отработало
```sh
k -n cert-manager describe certificate acme-letsencrypt
k -n cert-manager describe certificaterequest acme-letsencrypt-1
```
### Cloudflare
Если домен спрятан за CloudFlare могут возникнуть небольшие траблы
