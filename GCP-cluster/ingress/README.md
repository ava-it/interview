# INSTALL Ingress

```sh
kubectl apply -f ingress
```

### Test
```sh
kubectl delete -f ingress/ingress-example.yaml
kubectl port-forward --namespace=ingress-nginx service/ingress-nginx-controller 8080:80
curl --resolve demo.rke.teamlead.com:8080:127.0.0.1 http://demo.rke.teamlead.com:8080
```
