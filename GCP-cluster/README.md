# Install SDK Ubuntu 22.04
[LINK](https://cloud.google.com/sdk/docs/install#deb)

```sh
sudo apt-get install apt-transport-https ca-certificates gnupg

echo "deb [signed-by=/usr/share/keyrings/cloud.google.asc] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo tee /usr/share/keyrings/cloud.google.asc

sudo apt-get update && sudo apt-get install google-cloud-cli google-cloud-sdk-gke-gcloud-auth-plugin
```

# Авторизоваться и выбрать(создать проект)

`gcloud init`

```sh
gcloud projects create k-prod-efremov --name k-prod

gcloud config set project k-prod-efremov

gcloud services enable container.googleapis.com
```

# Create cluster master
```sh
gcloud container clusters create \
  --machine-type n1-standard-2 \
  --disk-size 20 \
  --num-nodes 1 \
  --zone europe-west3-a \
  --node-labels=master=true,control-plane=true \
  --node-locations europe-west3-a \
  --cluster-version latest \
  test-cluster
```

# Set kubectl creds
```sh
gcloud container clusters get-credentials test-cluster --zone europe-west3-a --project k-prod-efremov

kubectl get node
kubectl cluster-info
kubectl get all
```

### Give admin access

`kubectl create clusterrolebinding cluster-admin-binding --clusterrole=cluster-admin --user=efremov.ava.it@gmail.com`

# Create Cluster Services

```sh
gcloud container node-pools create workers \
    --machine-type e2-small \
    --disk-size 20 \
    --num-nodes 1 \
    --enable-autoscaling \
    --min-nodes 1 \
    --max-nodes 3 \
    --node-labels worker=true \
    --node-taints worker=true:NoSchedule \
    --zone europe-west3-a \
    --node-locations europe-west3-c \
    --cluster test-cluster

gcloud container node-pools create ingress \
    --machine-type e2-small \
    --disk-size 20 \
    --num-nodes 1 \
    --enable-autoscaling \
    --min-nodes 1 \
    --max-nodes 3 \
    --node-labels ingress=true \
    --node-taints ingress=true:NoSchedule \
    --zone europe-west3-a \
    --node-locations europe-west3-b \
    --cluster test-cluster

gcloud container node-pools create monitoring \
    --machine-type e2-small \
    --disk-size 20 \
    --num-nodes 1 \
    --enable-autoscaling \
    --min-nodes 1 \
    --max-nodes 3 \
    --node-labels monitoring=true \
    --node-taints monitoring=true:NoSchedule \
    --zone europe-west3-a \
    --node-locations europe-west3-b \
    --cluster test-cluster
```

## Install Gitlab-runner
gitlab-runner/README.md

## Install Ingress
ingress/README.md

## Install Prometheus Operator

```sh
# Download CRDs & Operator
curl -sL https://github.com/prometheus-operator/prometheus-operator/releases/download/v0.68.0/bundle.yaml -o monitoring/prometheus/bundle.yaml
# Create Namespace
kubectl create -f monitoring/prometheus/namespace.yaml
# Configure Operator Deployment (Tolerations, NodeSelectors, Namespace)
kubectl create -f monitoring/prometheus/operator.yaml
# Check If everything Ok with prometheus-operator
kubectl wait --for=condition=Ready pods -l  app.kubernetes.io/name=prometheus-operator -n monitoring
# Check prometheus-operator Logs
kubectl logs -l  app.kubernetes.io/name=prometheus-operator -n monitoring
kubectl create -f monitoring/prometheus/prometheus.yaml

kubectl create -f monitoring/prometheus/cadvissor.yaml
```

## Install Grafana

```sh
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
helm show values grafana/grafana --version 6.58.9 > monitoring/grafana/grafana.yaml
helm upgrade --install --values monitoring/grafana/grafana.yaml grafana grafana/grafana --version 6.58.9 --namespace monitoring

kubectl apply -f monitoring/grafana/ingress.yaml
```
### For local test

```sh
export GRAFANA_POD_NAME=$(kubectl get pods --namespace monitoring -l "app.kubernetes.io/name=grafana,app.kubernetes.io/instance=grafana" -o jsonpath="{.items[0].metadata.name}")
kubectl --namespace monitoring port-forward $GRAFANA_POD_NAME 3000
```
