# Запуск раннера в Kubernetes

## 1. Добавляем helm repo

```bash
helm repo add gitlab https://charts.gitlab.io
helm repo update

```

## 2. Установка gitlab-runner в кластер

Перед установкой нужно поправить файл с настройками: ```values.yaml```
обязательно `runnerRegistrationToken: <Токен для раннера>`

```bash
helm upgrade -i gitlab-runner gitlab/gitlab-runner -f gitlab/values.yaml -n gitlab-runner --create-namespace
```


kubectl --namespace $namespace get secrets

kubectl apply -f serviceAccount.gitlab-runner.yaml

kubectl --namespace $namespace get secret/${serviceAccount} -o jsonpath='{.data.token}'|base64 -d

debug runner
kubectl exec -it $(kubectl get pods -n gitlab-runner|grep -P "^ru"|awk '{ print$1 }') -n gitlab-runner -- bash

create docker registry secret
export CI_REGISTRY=
export CI_REGISTRY_USER=
export CI_REGISTRY_PASSWORD=
