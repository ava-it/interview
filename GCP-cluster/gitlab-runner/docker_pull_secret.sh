#!/bin/bash

NS=interview-app
REGISTRY=my-dokcer-registry

kubectl delete secret "${REGISTRY}" --namespace "${NS}"

kubectl create secret docker-registry "${REGISTRY}" \
  --docker-server "${CI_REGISTRY}" \
  --docker-email 'aleksandrvysk@mail.ru' \
  --docker-username "${CI_REGISTRY_USER}"\
  --docker-password "${CI_REGISTRY_PASSWORD}" \
  --namespace "${NS}"
