NS=interview-app
serviceAccount=gitlab-runner

echo "
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: ${serviceAccount}
  namespace: ${NS}
---
apiVersion: v1
kind: Secret
type: kubernetes.io/service-account-token
metadata:
  name: ${serviceAccount}
  namespace: ${NS}
  annotations:
    kubernetes.io/service-account.name: ${serviceAccount}
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: ${serviceAccount}
  namespace: ${NS}
rules:
- apiGroups: [\"\", \"extensions\", \"apps\", \"batch\", \"events\", \"networking.k8s.io\", \"certmanager.k8s.io\", \"cert-manager.io\", \"monitoring.coreos.com\", \"autoscaling\"]
  resources: [\"*\"]
  verbs: [\"*\"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: ${serviceAccount}
  namespace: ${NS}
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: ${serviceAccount}
subjects:
- kind: ServiceAccount
  name: ${serviceAccount}
  namespace: ${NS}
" > serviceAccount.${serviceAccount}.yaml
