## Install helm repo
helm repo add vm https://victoriametrics.github.io/helm-charts/

helm show values vm/victoria-metrics-agent >> vmagent-kubernetes.yml

helm --namespace=monitoring install vmagent vm/victoria-metrics-agent --create-namespace -f vmagent-kubernetes.yml
helm --namespace=monitoring upgrade --install vmagent vm/victoria-metrics-agent --create-namespace --values vmagent-kubernetes.yml
