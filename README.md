# Задача 1

## Установка Cluster/Resources
GCP-cluster/README.md

## Нагрузка на тестовое приложение
```sh
# 2xx
for i in {0..10000};do curl https://app.gcp.devopsit.ru;done
# 4xx
for i in {0..1000};do curl https://app.gcp.devopsit.ru/get4xx;done
# 5xx
for i in {0..1000};do curl https://app.gcp.devopsit.ru/get5xx;done
```

# Задача 2

## Первый запуск
make all

## Сборка
make build

## Запуск
make

### Проверка
любые изменения в `/app/pages/*` применяются.

# Задача 3

1. build == сборка и пуш в Registry
2. tests == любые тесты которые напишет разраб. Если для них нужны бд и т.д. все можно добавить.
3. test-fail == запускается руками, при запуске фейлится, триггерит джобу которая курлит апи PagerDuty.
    - по хорошему нужно было создать прогу на `GO` в дире пример `pagerDuty`.
    - Разбираться как работает PagerDuty не стал, никогда с ним не работал, аллерты в телегу приходят
4. failure == запускается при фейле одной из джоб.

# helm
добавлена helm роль которая запускает одно приложение через CI/CD
Именно на нем и роводим все тесты.
