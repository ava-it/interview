DEFAULD_GOUL := up
SHELL := /bin/bash

up: down
	docker compose up

down:
	docker compose down

build:	
	docker compose up build

all: build up

