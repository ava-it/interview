package main

import (
	"fmt"
	"log"

	"github.com/PagerDuty/go-pagerduty"
)

func main() {
	authtoken := "u+AK3rtJbG3918H1oevg"
	var opts pagerduty.ListUsersOptions
	client := pagerduty.NewClient(authtoken)

	if users, err := client.ListUsers(opts); err != nil {
		panic(err)
	} else {
		for _, p := range users.Users {
			fmt.Println(p)
			fmt.Println()
		}
	}

	event := pagerduty.Event{
		Type:        "trigger",
		ServiceKey:  "Q061H19Y0USM7O",
		Description: "Example event",
	}
	resp, err := pagerduty.CreateEvent(event)
	if err != nil {
		log.Println(resp)
		log.Fatalln("ERROR:", err)
	}
	log.Println("Incident key:", resp.IncidentKey)
}
