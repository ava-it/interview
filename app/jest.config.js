module.exports = {
    testEnvironment: 'jsdom',
    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.[jt]sx?$',
    transform: {
        '^.+\\.(js|jsx|ts|tsx)$': 'babel-jest',
      },
  };