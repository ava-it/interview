import { render, screen } from '@testing-library/react';
import Custom5XX from '../pages/get5XX';

test('Custom 5XX page renders correctly', () => {
  render(<Custom5XX />);
  const pageTitle = screen.getByText('Custom 5XX Page');
  const errorMessage = screen.getByText('Sorry, there was a server error.');

  expect(pageTitle).toBeInTheDocument();
  expect(errorMessage).toBeInTheDocument();
});

test('Custom 5XX page contains a link to the home page', () => {
  render(<Custom5XX />);
  const homePageLink = screen.getByText('Go back to the home page');
  expect(homePageLink).toBeInTheDocument();
});
