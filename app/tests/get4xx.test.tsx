import { render, screen } from '@testing-library/react';
import Custom404 from '../pages/get404';

test('Custom 404 page renders correctly', () => {
  render(<Custom404 />);
  const pageTitle = screen.getByText('Custom 404 Page');
  const errorMessage = screen.getByText('Sorry, the page you are looking for does not exist.');

  expect(pageTitle).toBeInTheDocument();
  expect(errorMessage).toBeInTheDocument();
});

test('Custom 404 page contains a link to the home page', () => {
  render(<Custom404 />);
  const homePageLink = screen.getByText('Go back to the home page');
  expect(homePageLink).toBeInTheDocument();
});
