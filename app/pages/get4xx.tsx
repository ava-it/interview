import { GetServerSideProps } from 'next';
import Link from 'next/link';

export default function get404() {
  return (
    <div>
      <h1>Another Page</h1>
      <p>This is a new page in your Next.js app.</p>
      <Link href="/">
        <a>Go back to the home page</a>
      </Link>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps = async () => {
  return {
    notFound: true, // Return a 404 status code
  };
};
