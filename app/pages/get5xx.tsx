import { GetServerSideProps } from 'next';

export default function CustomServerError() {
  return (
    <div>
      <h1>Custom 5XX Error Page</h1>
      <p>Sorry, something went wrong on our server.</p>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps = async ({ res }) => {
  // Simulate a 5XX server error (e.g., 500 Internal Server Error)
  res.statusCode = 500;
  return { props: {} };
};
